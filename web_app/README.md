# Trading Bot

Project created for Python laboratories

## Installation
Some cool conda features

- *set up conda environment*:
   ```terminal
   conda env create -f environment.yml
   ```

- *list environments*:
   ```terminal
   conda info --envs
   ```

- *activate environment*:
   ```terminal
   conda activate trade-bot
   ```


- *setup local variables*:
   You have to configure following environment variables: 
   - **DEBUG** - it shoud be set to true, otherwise server will be ran in non debug mode,
   - **DB_USER** - user to database, with which we want to connect with database,
   - **DB_PASS** - database user password to database,
   - **DB_NAME** - colection in database to which we want to connect to,
   - **DB_HOST** - host of database
   - **DB_PORT** - port of database

   Example:

   ```terminal
   conda env config vars set DEBUG=True DB_USER=admin DB_PASS=rutpassfort DB_NAME=admin DB_HOST=localhost DB_PORT=27018
   ```

   After that you reactivate environment:

   ```terminal
   conda deactivate   
   conda activate trade-server   
   ```

Step by step. change directory to web_app:
```
conda env create -f environment.yml && \
conda activate trade-server && \
conda env config vars set DEBUG=True DB_USER=admin DB_PASS=rutpassfort DB_NAME=admin DB_HOST=localhost DB_PORT=27018 && \
conda deactivate && \
conda activate trade-server
```

# Start server with:

```
python manage.py runserver
```

And go to [localhost:8000](localhost:8000)

name=web_app
conda env create -n $name -f environment.yml
conda init
conda activate $name
conda env config vars set DEBUG=True DB_USER=admin DB_PASS=rutpassfort DB_NAME=admin DB_HOST=localhost DB_PORT=27018
conda deactivate
conda activate $name

python manage.py runserver

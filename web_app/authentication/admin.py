from django.contrib import admin

from .forms import CustomerForm
from .models import Customer, Stock


class CustomerAdmin(admin.ModelAdmin):
    form = CustomerForm


admin.site.register(Stock)
admin.site.register(Customer, CustomerAdmin)

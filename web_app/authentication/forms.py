from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import Customer


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ["username", "email", "password1", "password2"]


class Balance(forms.Form):
    amount = forms.NumberInput()

    def clear_amount(self):
        amount_cleared = self.cleaned_data["amount"]

        return amount_cleared


class Stocks(forms.Form):
    stock = forms.CheckboxInput()


class CustomerForm(forms.ModelForm):
    username = forms.ModelChoiceField(
        queryset=User.objects.all(), empty_label="", to_field_name="username"
    )

    class Meta:
        model = Customer
        fields = "__all__"

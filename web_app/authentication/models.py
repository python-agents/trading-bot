from django import forms
from djongo import models


class Stock(models.Model):
    short_name = models.CharField(max_length=10)
    name = models.CharField(max_length=32)
    stock_exchange = models.CharField(max_length=10)

    objects = models.DjongoManager()

    def __str__(self):
        return self.short_name


class UserStocks(models.Model):
    short_name = models.CharField(max_length=100)
    amount = models.FloatField(default=0.0)
    invested = models.FloatField(default=0.0)

    class Meta:
        abstract = False

    def __str__(self):
        return self.short_name


class UserStockForm(forms.ModelForm):
    short_name = forms.ModelChoiceField(
        queryset=Stock.objects.all(), empty_label="", to_field_name="short_name"
    )

    class Meta:
        model = UserStocks
        fields = ("short_name", "amount", "invested")


class StatsOfDay(models.Model):
    day = models.DateField()
    state = models.FloatField(default=0.0)

    class Meta:
        abstract = False


class StatsOfDayForm(forms.ModelForm):
    day = forms.DateField(widget=forms.widgets.DateInput(attrs={"type": "date"}))

    class Meta:
        model = StatsOfDay
        fields = ("day", "state")


class Customer(models.Model):
    stocks = models.ArrayField(model_container=UserStocks, model_form_class=UserStockForm)
    states = models.ArrayField(model_container=StatsOfDay, model_form_class=StatsOfDayForm)

    username = models.CharField(max_length=100)
    wallet = models.FloatField(default=0.0)
    last_used_bot = models.CharField(max_length=4, default="day")
    bot_running = models.BooleanField(default=False)

    objects = models.DjongoManager()

    def str(self):
        return self.username

import json
import os

import requests
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render

from .forms import Balance, CreateUserForm, Stocks
from .models import Customer, Stock, UserStocks


def register(request):
    if request.user.is_authenticated:
        return redirect("home")

    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/login/")
    else:
        form = CreateUserForm()

    return render(request, "register/register.html", {"form": form})


def loginUser(request):
    if request.user.is_authenticated:
        return redirect("home")

    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("home")
        else:
            messages.error(request, "Wrong username or password")

    return render(request, "login/login.html")


def logoutUser(request):
    logout(request)
    return redirect("login")


@login_required(login_url="login")
def wallet(request):
    user = Customer.objects.get(username=request.user)
    messages = []

    if request.method == "POST":
        form = Balance(request.POST)
        if form.is_valid():
            amount = float(request.POST["amount"])
            if "add" in request.POST:
                if amount <= 0:
                    messages.append("Transaction failure")
                    messages.append("You have to enter positive value")
                else:
                    user.wallet += amount
                    messages.append("Transaction success")
                    user.save()
            elif "substract" in request.POST:
                if user.wallet - amount >= 0:
                    user.wallet -= amount
                    user.save()
                elif amount <= 0:
                    messages.append("Transaction failure")
                    messages.append("You have to enter positive value")
                else:
                    messages.append("Transaction failure")
                    messages.append("Too small amount on your balance")
            else:
                messages.append("Something went wrong")
    else:
        form = Balance()

    return render(
        request,
        "wallet/wallet.html",
        {"messages": messages, "balance": round(user.wallet, 2), "form": form},
    )


@login_required(login_url="login")
def home(request):
    user = Customer.objects.get_or_create(username=request.user)[0]

    # If user doesn't exists user object will look like (userObject, isSaved)
    # Otherwise it user is userObject
    messages = list()

    if request.method == "POST":
        form = Stocks(request.POST)
        if form.is_valid():
            if "add" in request.POST:
                selected_stock = Stock.objects.all().get(short_name=request.POST["stock-selected"])
                stock_to_add = UserStocks(short_name=str(selected_stock), amount=0, invested=0)
                if user.stocks is None:
                    user.stocks = [stock_to_add]
                    user.save()
                elif check_add_stock(user, stock_to_add):
                    user.stocks.append(stock_to_add)
                    user.save()
                else:
                    messages.append("Stock is already in use")

    balance = "----"
    wallet = round(user.wallet, 2)
    bot_running = user.bot_running
    bot_id = user.last_used_bot.upper()
    range_bot = (["RandomForest", "RDF"], ["NaiveStrategy", "NIV"], ["DayTrading", "DAY"])
    last30 = list()

    balanceHistory = get_balanceHistory(request.user)
    stocks = get_user_stocks(request.user)
    for stock in stocks:
        url = "http://%s:%s/last30/%s" % (
            os.environ["TRADE_HOST"],
            os.environ["TRADE_PORT"],
            stock[0],
        )
        response = requests.get(url)
        last30.append(response.json())

    def filter_used_stocks(stock):
        return stock.short_name not in list(map(lambda stock: stock[0], stocks))

    all_stocks_name = list(filter(filter_used_stocks, Stock.objects.all()))
    all_stocks = [[item.short_name, item.name] for item in all_stocks_name]
    range_invest = range(0, 101, 5)
    bot_presets = get_investments(request.user)

    return render(
        request,
        "home/home.html",
        {
            "user": user.username,
            "messages": messages,
            "balance": balance,
            "wallet": wallet,
            "bot_running": bot_running,
            "bot_id": bot_id,
            "range_bot": range_bot,
            "stocks": stocks,
            "stocks_js": json.dumps(stocks),
            "all_stocks": all_stocks,
            "last30": json.dumps(last30),
            "balanceHistory": balanceHistory,
            "range_invest": range_invest,
            "bot_presets": bot_presets,
        },
    )


def get_balanceHistory(username):
    data = Customer.objects.filter(username=username).values("states")[0]
    if data["states"] is not None:
        return [[value.state, str(value.day)] for value in data["states"]]
    else:
        return []


def get_user_stocks(username):
    data = Customer.objects.filter(username=username).values("stocks")[0]
    stocks = []

    if data["stocks"] is not None:
        for stock in data["stocks"]:
            stocks.append([stock.short_name, stock.amount, "----", "----"])

    return stocks


def get_investments(username):
    data = Customer.objects.filter(username=username).values("stocks")[0]
    stocks = []
    if data["stocks"] is not None:
        for stock in data["stocks"]:
            stocks.append([stock.short_name, stock.invested])

    return stocks


def check_add_stock(user, stock_to_add):
    if user.stocks is None:
        return True

    for stock in user.stocks:
        if stock.short_name == stock_to_add.short_name:
            return False
    return True

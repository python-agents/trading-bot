function focus() {
  [].forEach.call(this.options, function(o) {
    o.textContent = o.getAttribute('value') + ' (' + o.getAttribute('data-descr') + ')';
  });
}
function blur() {
  [].forEach.call(this.options, function(o) {
    o.textContent = o.getAttribute('value');
  });
}

[].forEach.call(document.querySelectorAll('.shortened-select'), function(s) {
  s.addEventListener('focus', focus);
  s.addEventListener('blur', blur);
  blur.call(s);
});

function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}

const timeFormat = 'DD/MM';
const colors = [['rgba(255, 99, 132, 1)', 'rgba(255, 99, 132, 0.2)'],
  ['rgba(75, 192, 192, 1)','rgba(75, 192, 192, 0.2)'],
  ['rgba(54, 162, 235, 1)','rgba(54, 162, 235, 0.2)'],
  ['rgba(252, 186, 3, 1)','rgba(252, 186, 3, 0.2)'],
  ['rgba(157, 252, 3, 1)','rgba(157, 252, 3, 0.2)'],
  ['rgba(252, 132, 3, 1)','rgba(252, 132, 3, 0.2)'],
  ['rgba(235, 3, 252, 1)','rgba(235, 3, 252, 0.2)'],
  ['rgba(110, 45, 189, 1)','rgba(110, 45, 189, 0.2)'],
]

function newDateString(date) {
  return moment(date, 'YYYY-MM-DD').format(timeFormat);
}

function createDatesX(days) {
  return days.map(row => newDateString(row.date));
}

function roundNumbers(array) {
  return array.map(item => item.toFixed(2));
}

function rowReturner(array, row) {
  return array.slice(array.length - 30).map(item => item[row]);
}

function createChart(stocks, last30, balanceHistory) {
  const array = [{
    label: 'Balance',
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderColor: 'rgba(255, 255, 255, 1)',
    fill: false,
    data: rowReturner(balanceHistory, 0),
  }];

  stocks.forEach((stock, index) => {
    numbers = roundNumbers(last30[index].map(item => item.value));
    array.push({
      label: stock[0],
      backgroundColor: colors[index % 8][1],
      borderColor: colors[index % 8][0],
      fill: false,
      data: numbers
    })
  });  

  const config = {
    type: 'line',
    data: {
      labels: createDatesX(last30[0]),
      datasets: array,
    },
    options: {
      title: {
        text: 'History'
      },
      maintainAspectRatio: false,
      scales: {
        x: {
          type: 'time',
          time: {
            parser: timeFormat,
            // round: 'day'
            tooltipFormat: 'll HH:mm'
          },
          scaleLabel: {
            display: true,
            labelString: 'Date'
          }
        },
        y: {
          scaleLabel: {
            round: 'Amount',
            display: true,
            labelString: 'Amount'
          }
        }
      },
    }
  };

  window.onload = function() {
    var ctx = document.getElementById('myChart1').getContext('2d');
    window.myLine = new Chart(ctx, config);
  };
}

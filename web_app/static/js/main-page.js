const wallet = document.querySelector('#your-wallet');
const balance = document.querySelector('#your-balance');
const balanceArrow = document.querySelector("#balance-arrow");
const stocksRows = document.querySelectorAll("#stocks > tr");
let totalStockValues = 0;


function getBalance() {
         let wallet_state = wallet.innerHTML.replace('$', '');
         if (balance.innerHTML.replace('$', '') == "----") {
            const previousBalance = 0
         }
         else {
            const previousBalance = balance.innerHTML.replace('$', '');
         }
         const balanceState = Number(wallet_state) + totalStockValues
         balance.innerHTML = `${balanceState.toFixed(2)}$`;

         if (previousBalance <= balanceState) {
            balanceArrow.classList.replace('fa-angle-double-down', 'fa-angle-double-up');
            balanceArrow.classList.replace('arrow-down', 'arrow-up');
         } else {
            balanceArrow.classList.replace('fa-angle-double-up', 'fa-angle-double-down');
            balanceArrow.classList.replace('arrow-up', 'arrow-down');
         }
}


function getWallet() {
   fetch('/update/wallet-state', { credentials: 'same-origin' })
      .then(response => response.json())
      .then(data => {
         const wallet_state = data.wallet.toFixed(2);
         const previousWallet = wallet.innerHTML.replace('$', '');
         wallet.innerHTML = `${wallet_state}$`;

         if (previousWallet <= wallet_state) {
            balanceArrow.classList.replace('fa-angle-double-down', 'fa-angle-double-up');
            balanceArrow.classList.replace('arrow-down', 'arrow-up');            
         } else {
            balanceArrow.classList.replace('fa-angle-double-up', 'fa-angle-double-down');
            balanceArrow.classList.replace('arrow-up', 'arrow-down');            
         }
      });
}

function getUserQuantity() {
   fetch('/update/quantities', { credentials: 'same-origin' })
      .then(response => response.json())
      .then(data => {
         for (const stock of stocksRows) {
            item = data.find(item => item.short_name === stock.cells[0].innerHTML)

            if (item) {
               stock.cells[1].innerHTML = item.amount;
            }
         }
      });
}

function getStocksValues() {
   totalStockValues = 0;
   for (const stock of stocksRows) {
      const [symbol, qty, value, change] = stock.cells;

      fetch(`/update/get-price/${symbol.innerHTML}`)
         .then(response => response.json())
         .then(data => {
            value.innerHTML = data.current_price;
            change.innerHTML = `${(data.change * 100).toFixed(2)}%`;
            change.classList.toggle("td-plus", data.change >= 0);
            change.classList.toggle("td-minus", data.change < 0);
            totalStockValues += Number(qty.innerHTML) * Number(data.current_price);
            getBalance();
         }).catch(error => {
            console.log("Error during downloading stocks info");
         });
   }
}

function updateInvestments(name, newValue) {
   newValue = newValue.replace('%', '');
   fetch(`/update/invests/${name}/${newValue}`)
      .then(response => {
         if (response.status === 400) {
            alert("Sum of investments cannot be over 100%");
            window.location.reload(true);
         }
      });
}

function deleteStock(name) {
   fetch(`/update/delete-stock/${name}`).then(() => {location.reload()});
}

function startBot() {
  const botType = document.getElementById("bot-type").value;
  fetch(`/update/start-bot/${botType}`);
}

function stopBot() {
  fetch(`/update/stop-bot`);
}

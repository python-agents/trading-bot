import os

import requests
from authentication import models
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt


@login_required(login_url="login")
def wallet_state(request):
    user = models.Customer.objects.get(username=request.user)
    return JsonResponse({"wallet": round(user.wallet, 2)})


@login_required(login_url="login")
def get_quantities(request):
    data = models.Customer.objects.filter(username=request.user).values("stocks")[0]
    stocks = []
    if data["stocks"] is not None:
        for stock in data["stocks"]:
            stocks.append({"short_name": stock.short_name, "amount": stock.amount})
    return JsonResponse(stocks, safe=False)


@login_required(login_url="login")
def update_investments(request, name, newValue):
    user = models.Customer.objects.get(username=request.user)
    stock_to_edit = list(filter(lambda stock: stock.short_name == name, user.stocks))[0]
    stock_to_edit.invested = newValue

    if sum(map(lambda stock: int(stock.invested), user.stocks)) > 100:
        return HttpResponseBadRequest("Summary investments is bigger than 100%")
    else:
        user.save()
        return HttpResponse("Edited successfully")


@csrf_exempt
def get_stock_value(request, stock):
    url = "http://%s:%s/get-price/%s" % (os.environ["TRADE_HOST"], os.environ["TRADE_PORT"], stock)
    response = requests.get(url)

    if response:
        return JsonResponse(response.json())
    else:
        return HttpResponse("")


@login_required(login_url="login")
def delete_stock(request, stock):
    user = models.Customer.objects.get_or_create(username=request.user)
    if not user[1]:
        user = user[0]
        user.save()
    stock_to_delete = ""
    for x in user.stocks:
        if x.short_name == stock:
            stock_to_delete = x
            break
    user.stocks.remove(stock_to_delete)
    user.save()
    return HttpResponse()


@login_required(login_url="login")
def start_bot(request, bot_type):
    user = models.Customer.objects.get(username=request.user)
    stocks = list(filter(lambda stock: stock.invested > 0, user.stocks))

    payload = {"user": user.username, "strategy": bot_type.lower(), "short_stock": {}}

    for stock in stocks:
        payload["short_stock"][stock.short_name] = stock.invested / 100

    url = "http://%s:%s/bot/start" % (os.environ["TRADE_HOST"], os.environ["TRADE_PORT"])

    if len(payload["short_stock"]) > 0:
        response = requests.post(url, json=payload)
        return HttpResponse(response)
    else:
        return HttpResponse("No stocks selected")


@login_required(login_url="login")
def stop_bot(request):
    url = "http://%s:%s/bot/stop" % (os.environ["TRADE_HOST"], os.environ["TRADE_PORT"])
    response = requests.post(url, json={"user": request.user.username})
    return HttpResponse(response)

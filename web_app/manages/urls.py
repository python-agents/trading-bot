from django.urls import path
from manages import views as v


urlpatterns = [
    path("wallet-state/", v.wallet_state, name="wallet_state"),
    path("get-price/<slug:stock>", v.get_stock_value, name="stock_value"),
    path("quantities", v.get_quantities, name="stock_value"),
    path("invests/<slug:name>/<slug:newValue>", v.update_investments, name="stock_value"),
    path("delete-stock/<slug:stock>", v.delete_stock, name="delete_stock"),
    path("start-bot/<slug:bot_type>", v.start_bot, name="start_bot"),
    path("stop-bot", v.stop_bot, name="stop_bot"),
]

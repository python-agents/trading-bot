import datetime as dt
import time

from classes.live_trading.frequencies import freq_to_sec
from classes.live_trading.PersonalAccount import PersonalAccount
from classes.live_trading.strategies.strategy_symbols import STRATEGIES


class TradeBot(object):
    def __init__(self, stock_names, user_id, stock_percent, database, strategy_name="day"):
        self.database = database
        self.broker = PersonalAccount(user_id, database=self.database, stock_percent=stock_percent)
        self._add_stock_to_database(user_id, stock_names)
        self.strategy_class = self._create_strategy(strategy_name)
        self.strategy = self.strategy_class(stock_names, self.broker, self)
        self.running = True

    def _create_strategy(self, name):
        return STRATEGIES[name]

    def run(self):
        """Bot runs only if NYSE is open"""
        _frequency = freq_to_sec(self.strategy.frequency)
        while self.running:
            if 16 <= dt.datetime.now().hour <= 22:
                self.strategy.handle_data()
            _sleeper = 0
            while self.running and _sleeper < _frequency:
                _sleeper += 1
                time.sleep(1)

    def stop(self):
        self.running = False

    def update_wallet(self, new_wallet):
        self.broker.update_wallet(new_wallet)

    def _add_stock_to_database(self, user_id, stock_names):
        for stock_name in stock_names:
            self.database.add_stock_to_users(user_id, stock_name)
            self.database.add_holder(stock_name, user_id)

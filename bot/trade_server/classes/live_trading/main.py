import argparse
import time

from classes.live_trading.frequencies import freq_to_sec
from classes.live_trading.PersonalAccount import PersonalAccount
from classes.live_trading.strategies.strategy_symbols import STRATEGIES


def create_strategy(name):
    return STRATEGIES[name]


def run_strategy(strategy):
    while True:
        strategy.handle_data()
        time.sleep(freq_to_sec(strategy.frequency))


def main():
    """
    example:
       $ ./main AAPL MSFT --strategy rdf --user_id 1
    if we want to run a test:
       $ ./main --strategy rdf --test
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--test", action="store_true")
    parser.add_argument("stock_names", nargs="*", metavar="stocks", type=str)
    parser.add_argument("--strategy", nargs=1, metavar="strategy")
    parser.add_argument("--user_id", nargs=1, metavar="user_id")
    args = parser.parse_args()
    strategy_class = create_strategy(*args.strategy)
    if not args.test:
        broker = PersonalAccount(args.user_id)
        strategy = strategy_class(args.stocks, broker)
    else:
        broker = PersonalAccount(-1, is_dummy=True)
        strategy = strategy_class(["AAPL", "TSLA", "MSFT"], broker)

    run_strategy(strategy)


if __name__ == "__main__":
    main()

from datetime import datetime

from classes.live_trading.DataFetcher import DataFetcher


class PersonalAccount:
    """
    Class responsible for holding information about the broker:
    - his amount of money (wallet)
    - number of his stock shares
    Also each PersonalAccount object can order to buy / sell an
    amount of stocks.
    """

    def __init__(self, user_id, database=None, is_dummy=False, wallet=100000.0, stock_percent=None):
        """
        PersonalAccount during tests should be instantiated with is_dummy=True
        and wallet=(as much as you want)
        When program works regularly should be instantiated as PersonalAccount(user_id)
        :param user_id: Primary key in database
        :param is_dummy: Boolean value, which should be true
        only if the instance is created for testing purpose.
        :param wallet: Starting amount of money.
        :param stock_percent: dictionary which informs how much money do we want to invest in each
               stock. Key should be the stock symbol. Value should be float in range 0.0 - 1.0
        """

        self.database = database
        self.user_id = user_id
        self.stock_percent = stock_percent
        self.is_dummy = is_dummy
        if is_dummy:
            if stock_percent is None:
                self.stock_percent = {"AAPL": 0.5, "MSFT": 0.3, "TSLA": 0.2}
            self.wallet = wallet
            """ stock_budget is partition of wallet for each stock we want to deal with"""
            self.stock_budget = {
                stock: self.stock_percent[stock] * wallet for stock in self.stock_percent
            }
            """ stocks should be in form of the dictionary with stock names as key
            and possessed amount of each one as integer """
            self.own_stocks = {"AAPL": 12, "MSFT": 8, "TSLA": 5}
        else:
            self._update_user()

    def _update_user(self):
        """
        Method used to set wallet (amount of money)
        and own_stocks (type of possessed stocks and amount of each)
        according to database records matching on user_id.
        """
        self.wallet = self.database.get_wallet_value(self.user_id)
        # initialize budget for particular stocks
        self.stock_budget = {
            stock: self.stock_percent[stock] * self.wallet for stock in self.stock_percent
        }
        _stocks = self.database.get_user_stocks(self.user_id)
        _own_stocks = {}
        for stock in _stocks:
            _own_stocks[stock["short_name"]] = stock["amount"]
        self.own_stocks = _own_stocks

    def _update_database(self):
        """
        Method used to update database with new wallet and own_stocks values.
        """
        self.database.update_wallet(self.user_id, self.wallet)
        for stock_name in self.own_stocks:
            self.database.update_stock_amount(self.user_id, stock_name, self.own_stocks[stock_name])
            self.database.update_holder_share(stock_name, self.user_id, self.own_stocks[stock_name])

    def calculate_account_total_value(self):
        shares_total = sum(
            [self.own_stocks[s] * DataFetcher.live_selling_price(s) for s in self.own_stocks]
        )
        return float(format(shares_total + self.wallet, ".2f"))

    def calculate_wallet_total_value(self):
        money_total = sum([self.stock_budget[s] for s in self.stock_budget])
        return float(format(money_total, ".2f"))

    def order(self, stock_symbol, amount):
        """
        Method used to sell or buy stocks.
        :param stock_symbol: ex. 'AAPL'
        :param amount: nonzero integer - positive when we want to buy amount of stocks,
                                         negative if we want to sell amount of stocks.
        """

        if amount > 0:
            total_cost = float(format(DataFetcher.live_buying_price(stock_symbol) * amount, ".2f"))
        else:
            total_cost = float(format(DataFetcher.live_selling_price(stock_symbol) * amount, ".2f"))

        # error if we buy more stocks than we can afford
        if amount > 0 and self.stock_budget[stock_symbol] < total_cost:
            self._print_not_enough_money_log(total_cost, stock_symbol, amount)
            return
        # error if we sell stocks we don't have at all
        elif amount < 0 and stock_symbol not in self.own_stocks:
            self._print_not_enough_shares_log(stock_symbol, amount)
            return
        # error if we sell more stocks than we actually have
        elif amount < 0 and self.own_stocks[stock_symbol] < abs(amount):
            self._print_not_enough_shares_log(stock_symbol, amount)
            return

        else:
            self.stock_budget[stock_symbol] -= total_cost
            if stock_symbol in self.own_stocks:
                self.own_stocks[stock_symbol] += amount
            else:
                self.own_stocks[stock_symbol] = amount

        # amount of money should be two decimal digit
        self.stock_budget[stock_symbol] = float(format(self.stock_budget[stock_symbol], ".2f"))
        # change info
        self.wallet = self.calculate_wallet_total_value()
        self._print_success_order_log(total_cost, stock_symbol, amount)
        # send changes in user's account to database
        if not self.is_dummy:
            self._update_database()

    def _print_success_order_log(self, total_cost, stock_symbol, amount):
        print(
            "{5}\n"
            "ordered {0} shares of {1} for ${2}\n"
            "wallet = ${3}\t{1} shares = {4}\n".format(
                amount,
                stock_symbol,
                abs(total_cost),
                self.wallet,
                self.own_stocks[stock_symbol],
                datetime.now(),
            )
        )

    def _print_not_enough_money_log(self, total_cost, stock_symbol, amount):
        print(
            "{}\n"
            "not enough money in this stock budget\n"
            "couldn't order {} shares of {}\n"
            "wallet = {}\ttotal_cost = {}\n"
            "this stock budget = {}\n".format(
                datetime.now(),
                amount,
                stock_symbol,
                self.wallet,
                abs(total_cost),
                self.stock_budget[stock_symbol],
            )
        )

    def _print_not_enough_shares_log(self, stock_symbol, amount):
        if stock_symbol in self.own_stocks:
            number_of_shares = self.own_stocks[stock_symbol]
        else:
            number_of_shares = 0

        print(
            "{0}\n"
            "not enough shares\n"
            "couldn't sell {1} shares of {2}\n"
            "{2} shares = {3}\n".format(datetime.now(), amount, stock_symbol, number_of_shares)
        )

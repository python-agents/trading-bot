import re


def freq_to_sec(frequency_str):
    """
    Function which takes frequency symbol - ex. '12m' for twelve minutes
    or '45s' for forty-five seconds - and returns equivalent number of seconds as integer.
    :param frequency_str: non-spaced string starting with an integer followed by a letter s/m/h/d/w
           which stands for second/ minute/ hour/ day/ week.
    :return: Equivalent number of seconds (integer).
    """
    time_symbols = {"s": 1, "m": 60, "h": 60 * 60, "d": 24 * 60 * 60, "w": 7 * 24 * 60 * 60}
    t = re.split(r"(\d+)", frequency_str)
    return int(t[1]) * time_symbols[t[2]]

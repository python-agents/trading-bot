import time

from classes.live_trading.DataFetcher import DataFetcher
from classes.live_trading.strategies.BaseStrategy import BaseStrategy


class DayTrading(BaseStrategy):
    """
    Strategy purpose is to work during the trading day. Current stock price is checked regularly.
    Orders to buy shares when there are three consecutive price rises. Order to sell otherwise.
    """

    def __init__(self, stock_symbols, broker, bot, frequency="20s"):
        super().__init__(stock_symbols, broker, bot, frequency)
        self.last_prices = {symbol: [] for symbol in self.stock_symbols}

    def handle_data(self):
        for stock_symbol in self.stock_symbols:

            # initialize last_prices when calling handle_data for the 1st time
            if not self.last_prices[stock_symbol]:
                self.last_prices[stock_symbol].append(DataFetcher.live_buying_price(stock_symbol))
                while len(self.last_prices[stock_symbol]) < 3 and self.bot.running is True:
                    for i in range(3):
                        time.sleep(9)
                        live_price = DataFetcher.live_buying_price(stock_symbol)
                        if live_price != self.last_prices[stock_symbol][-1]:
                            self.last_prices[stock_symbol].append(live_price)
                            break
                        elif self.bot.running is False:
                            return
            # buy when three consecutive price rises occur
            if self._value_is_increasing(stock_symbol):
                self.broker.order(stock_symbol, 2)
            # sell when three consecutive price drops occur
            elif self._value_is_descending(stock_symbol):
                self.broker.order(stock_symbol, -3)
            # update last prices
            del self.last_prices[stock_symbol][0]
            self.last_prices[stock_symbol].append(DataFetcher.live_buying_price(stock_symbol))

    def _compare_iter(self, stock_symbol):
        return zip(self.last_prices[stock_symbol], self.last_prices[stock_symbol][1:])

    def _value_is_descending(self, stock_symbol):
        if all(i > j for i, j in self._compare_iter(stock_symbol)):
            return True
        return False

    def _value_is_increasing(self, stock_symbol):
        if all(i <= j for i, j in self._compare_iter(stock_symbol)) and not all(
            i == j for i, j in self._compare_iter(stock_symbol)
        ):
            return True
        return False

from pathlib import Path

import numpy as np
from classes.live_trading.DataFetcher import DataFetcher
from classes.live_trading.strategies.BaseStrategy import BaseStrategy
from joblib import load


class RandomForest(BaseStrategy):
    """
    Representation of trading strategy based on ML. It makes decisions based on stock's
    closing price value of last 32 days and tries to forecast stock's closing price for
    next 7 days. Then it compares average closing price from last 32 days and maximum
    price from forecast. If the first one is greater - order to sell amount of shares.
    Buy amount of shares otherwise.
    """

    lag = 33
    forecast = 7
    frequency = "1d"
    # NOTE: It's a relative path from live_trading/main.py and instantiating
    #       RandomForest from this file is impossible.
    PATH_TO_MODEL = Path("./classes/live_trading/strategies/rnd_forest.joblib")

    def __init__(self, stock_symbols, broker, bot, frequency="1d"):
        """
        :param stock_symbols: list of stock symbols to trade with ex.: ['AAPL', 'TSLA']
        :param broker: instance of PersonalAccount class
        """
        super().__init__(stock_symbols, broker, bot, frequency)
        # need to train model first and give its path below
        self.rf_model = load(self.PATH_TO_MODEL)

    def handle_data(self):
        for stock_symbol in self.stock_symbols:

            # take closing stock values of last lag=33 days
            historical_closing = DataFetcher.last_months_closing_price(stock_symbol)[-self.lag :]
            # predict the closing values for next forecast=7 days
            predictions = self.rf_model.predict(historical_closing.reshape(1, -1))

            max_predicted = np.max(predictions)
            historical_mean = np.mean(historical_closing)

            # make economic decision -> go 'out' or 'in'.
            # TODO: Make sth like risk management, so it's not always amount=10
            if max_predicted > historical_mean:
                self.broker.order(stock_symbol, 10)
            if max_predicted < historical_mean:
                self.broker.order(stock_symbol, -10)

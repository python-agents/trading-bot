from abc import ABC


class BaseStrategy(ABC):
    """
    Abstract class which is a pattern for a trading strategy class
    """

    def __init__(self, stock_symbols, broker, bot, frequency):
        self.stock_symbols = stock_symbols
        self.broker = broker
        self.frequency = frequency
        self.bot = bot

    def handle_data(self):
        """
        Method which is called by program in intervals described in "frequency" field.
        It performs economic analysis and decisions based on them. This method should
        call "broker's" method order(stock_symbol, amount).
        :return:
        """
        pass

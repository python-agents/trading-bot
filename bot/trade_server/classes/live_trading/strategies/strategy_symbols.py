from classes.live_trading.strategies.DayTrading import DayTrading
from classes.live_trading.strategies.NaiveStrategy import NaiveStrategy
from classes.live_trading.strategies.RandomForest import RandomForest


"""
Constant symbols which stands for class representation of a certain strategy
"""
STRATEGIES = {"rdf": RandomForest, "niv": NaiveStrategy, "day": DayTrading}

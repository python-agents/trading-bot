from classes.live_trading.strategies.BaseStrategy import BaseStrategy


class NaiveStrategy(BaseStrategy):
    """
    Strategy just buys amount of stocks every 30 seconds.
    Purpose of its existence is presentation of how does
    a trading strategy work.
    """

    def __init__(self, stock_symbols, broker, bot, frequency="30s"):
        super().__init__(stock_symbols, broker, bot, frequency)

    def handle_data(self):
        for stock_symbol in self.stock_symbols:
            self.broker.order(stock_symbol, 1)

from datetime import date

import yfinance as yf


def _set_start_date(months_ago=2):
    today = date.today()
    t_day = today.day
    t_month = today.month
    t_year = today.year
    new_month = t_month - months_ago

    if new_month < 1:
        t_year -= 1
        new_month = 12 + new_month

    if t_day > 28:
        if new_month == 2:
            t_day = 28
        elif t_day == 31:
            t_day = 30

    return today.replace(year=t_year, month=new_month, day=t_day)


class DataFetcher(object):
    _fields = ["bid", "ask"]

    @staticmethod
    def _fetch(stock_symbol):
        obligation = yf.Ticker(stock_symbol)
        stats = {}
        for field in DataFetcher._fields:
            # sometimes it returns dataframe with value hidden in 'raw'
            if type(obligation.info[field]) is dict:
                stats[field] = obligation.info[field]["raw"]
            else:
                stats[field] = obligation.info[field]
        return stats

    @staticmethod
    def live_buying_price(stock_symbol):
        """
        Method finds the 'ask' price, which is the lowest price, that a seller
        would want to be paid for a single share.
        :param stock_symbol: for example 'AAPL' or 'MSFT'
        :return: ask price for the observed stock
        """
        return float(DataFetcher._fetch(stock_symbol)["ask"])

    @staticmethod
    def live_selling_price(stock_symbol):
        """
        Method finds the 'bid' price, which is the highest price, that a buyer
        would pay for a single share.
        :return: bid price for the observed stock
        :param stock_symbol: for example 'AAPL' or 'MSFT'
        """
        return float(DataFetcher._fetch(stock_symbol)["bid"])

    @staticmethod
    def last_months_closing_price(stock_symbol, months_ago=2):
        """
        Method downloads the historical 'close' prices of given stock from past
        months_ago months to today.
        :param stock_symbol: for example 'AAPL' or 'MSFT'
        :param months_ago: number of months ago from today to start interval
        :return: nd.array of closing values of stocks from past months
        """
        pd_historical_data = yf.download(stock_symbol, start=_set_start_date(months_ago=months_ago))
        return pd_historical_data["Close"].values

    @staticmethod
    def stocks_closing_price(stock_symbols, months_ago=1):
        pd_historical_data = yf.download(
            stock_symbols, start=_set_start_date(months_ago=months_ago)
        )
        return pd_historical_data["Close"]

    @staticmethod
    def last_months_with_data(stock_symbol, months_ago=2):
        pd_historical_data = yf.download(stock_symbol, start=_set_start_date(months_ago=months_ago))
        return [pd_historical_data["Close"].values, pd_historical_data.index]

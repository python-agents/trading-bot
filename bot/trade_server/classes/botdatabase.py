import os

from pymongo import MongoClient


class Database:
    def __init__(self):
        self.client = MongoClient(
            os.environ["DB_HOST"],
            int(os.environ["DB_PORT"]),
            username=os.environ["DB_USER"],
            password=os.environ["DB_PASS"],
            authMechanism="SCRAM-SHA-1",
            maxPoolSize=250,
        )
        self.db = self.client[os.environ["DB_NAME"]]
        self.stocks = self.db.authentication_stock
        self.users = self.db.authentication_customer

    def add_stock(self, stock_name, short_name, stock_exchange):
        if not self.check_if_stocks_exist(short_name):
            new_stock = {
                "stock_name": stock_name,
                "short_name": short_name,
                "stock_exchange": stock_exchange,
                "holders": [],
            }
            self.stocks.insert_one(new_stock)

    def check_if_stocks_exist(self, short_name):
        cursor = self.stocks.find({"short_name": short_name}, {"_id": 0})
        if len(list(cursor)) > 0:
            return True
        else:
            return False

    def check_if_holder_exist(self, short_stock, holder_name):
        cursor = self.stocks.find({"short_name": short_stock, "holders.name": holder_name})
        if len(list(cursor)) > 0:
            return True
        else:
            return False

    def check_if_user_exist(self, short_name):
        users_list = self.users.find({"username": short_name}, {"_id": 0})
        if len(list(users_list)) > 0:
            return True
        else:
            return False

    def find_stock_info(self):
        cursor = self.stocks.find({}, {"_id": 0, "holders": 0, "history": 0})
        results = []
        for i in cursor:
            results.append(i)
        return results

    def add_holder(self, short_stock, user, share=0):
        if not self.check_if_holder_exist(short_stock, user):
            query = {"short_name": short_stock}
            execution = {"$push": {"holders": {"name": user, "share": share}}}
            self.stocks.update_one(query, execution)

    def update_holder_share(self, short_stock, holder, update_value):
        query = {"short_name": short_stock, "holders.name": holder}
        execution = {"$set": {"holders.$.share": update_value}}
        self.stocks.update_one(query, execution)

    def delete_holder(self, short_stock, user):
        query = {"short_name": short_stock}
        execution = {"$pull": {"holders": {"name": user}}}
        self.stocks.update_one(query, execution)

    def get_stock_list(self):
        cursor = self.stocks.find()
        result = []
        for i in cursor:
            result.append({"name": i["name"], "short_name": i["short_name"]})
        return result

    def find_state(self, short_name, holder_name):
        cursor = self.stocks.find(
            {"short_name": short_name},
            {
                "_id": 0,
                "short_name": 0,
                "stock_name": 0,
                "holders": {"$elemMatch": {"name": holder_name}},
            },
        )
        state = [record["holders"] for record in cursor]
        return state

    def check_if_stock_in_users(self, username, short_stock):
        cursor = self.users.find({"username": username, "stocks.short_name": short_stock})
        if len(list(cursor)) > 0:
            return True
        else:
            return False

    def add_stock_to_users(self, username, short_stock, amount=0):
        if not self.check_if_stock_in_users(username, short_stock):
            query = {"username": username}
            execution = {"$push": {"stocks": {"short_name": short_stock, "amount": amount}}}
            self.users.update_one(query, execution)

    def get_stocks_short_names(self):
        return list(map(lambda stock: stock["short_name"], self.stocks.find()))

    def update_stock_field(self, stock, field_name, value):
        query = {"short_name": stock}
        execution = {"$set": {field_name: value}}
        self.stocks.update_one(query, execution)

    def update_bot_state(self, user, bot_type=None):
        if bot_type is None:
            execution = {"$set": {"bot_running": False}}
        else:
            execution = {"$set": {"bot_running": True, "last_used_bot": bot_type}}

        self.users.update_one({"username": user}, execution)

    def get_last_month_history(self, short_name):
        last_closing_price = self.stocks.find({"short_name": short_name}, {"history": 1})
        return list(last_closing_price)[0]["history"]

    def get_last_closing_price(self, short_name):
        last_closing_price = self.stocks.find(
            {"short_name": short_name}, {"_id": 1, "history": {"$slice": -1}}
        )
        return list(last_closing_price)[0]["history"][0]["value"]

    def update_stock_amount(self, username, short_stock, new_value):
        query = {"username": username, "stocks.short_name": short_stock}
        execution = {"$set": {"stocks.$.amount": new_value}}
        self.users.update_one(query, execution)

    def get_stock_amount_from_user(self, username, short_stock):
        cursor = self.users.find(
            {"username": username}, {"stocks": {"$elemMatch": {"short_name": short_stock}}}
        )
        return cursor[0]["stocks"][0]

    def update_wallet(self, username, new_value):
        query = {"username": username}
        execution = {"$set": {"wallet": new_value}}
        self.users.update_one(query, execution)

    def get_wallet_value(self, username):
        cursor = self.users.find({"username": username})
        return cursor[0]["wallet"]

    def get_user_stocks(self, username):
        user_list = self.users.find({"username": username})
        return user_list[0]["stocks"]

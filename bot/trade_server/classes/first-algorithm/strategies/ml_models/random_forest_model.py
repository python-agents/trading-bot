import matplotlib.pyplot as plt
import pandas as pd
from joblib import dump
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from zipline.data import bundles
from zipline.data.data_portal import DataPortal
from zipline.utils.calendars import get_calendar


bundle_data = bundles.load("quandl")
end_date = pd.Timestamp("2020", tz="utc")

first_trading_day = bundle_data.equity_daily_bar_reader.first_trading_day

data_por = DataPortal(
    asset_finder=bundle_data.asset_finder,
    trading_calendar=get_calendar("NYSE"),
    first_trading_day=bundle_data.equity_daily_bar_reader.first_trading_day,
    equity_daily_reader=bundle_data.equity_daily_bar_reader,
)

AAPL = data_por.asset_finder.lookup_symbol("AAPL", as_of_date=None)

dataframe = data_por.get_history_window(
    assets=[AAPL],
    end_dt=end_date,
    bar_count=5000,
    frequency="1d",
    data_frequency="daily",
    field="close",
)

dataframe = dataframe.dropna()
dataframe.index = pd.DatetimeIndex(dataframe.index)
dataframe["close"] = dataframe[list(dataframe.columns)[0]]
dataframe = dataframe.drop(columns=[list(dataframe.columns)[0]])
dataframe["1d"] = dataframe.shift(-1)

for day in range(2, 41):
    col = "%dd" % day
    dataframe[col] = dataframe["close"].shift(-1 * day)
dataframe.head()

X = dataframe.iloc[:, :33]
y = dataframe.iloc[:, 33:]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4)

regressor = RandomForestRegressor(
    n_estimators=100,
    criterion="mse",
    max_depth=None,
    min_samples_split=2,
    min_samples_leaf=1,
    min_weight_fraction_leaf=0.0,
    max_features="auto",
    max_leaf_nodes=None,
    min_impurity_decrease=0.0,
    min_impurity_split=None,
    bootstrap=True,
    oob_score=True,
    n_jobs=16,
    random_state=None,
    verbose=1,
    warm_start=False,
)

X_train = X_train.fillna(0)
y_train = y_train.fillna(0)
X_test = X_test.fillna(0)
y_test = y_test.fillna(0)

regressor.fit(X_train, y_train)

dump(regressor, "rf_regressor.joblib")

print(regressor.score(X_test, y_test))

y_predicted = regressor.predict(X_test)

fig, ax = plt.subplots()

fig.subplots_adjust(bottom=0.3)

fig.set_figwidth(16)
fig.set_figheight(8)

ax.plot(y_test.index, y_test["40d"], "ro")
ax.plot(y_test.index, y_predicted[:, 7], "bo")

ax.legend()

plt.xticks(rotation=70)
plt.tight_layout()
plt.show()

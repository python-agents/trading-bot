from os import environ

from strategies.buy_and_hold import BuyAndHold
from toolz import merge
from zipline import run_algorithm
from zipline.utils.calendars import get_calendar, register_calendar


# Columns that we expect to be able to reliability deterministic
# Doesn't  include fields that have UUIDS.
_cols_to_check = [
    "algo_volatility",
    "algorithm_period_return",
    "alpha",
    "benchmark_period_return",
    "benchmark_volatility",
    "beta",
    "capital_used",
    "ending_cash",
    "ending_exposure",
    "ending_value",
    "ending_value",
    "ecxess_return",
    "gross_leverage",
    "long_exposure",
    "long_value",
    "longs_count",
    "max_drawdown",
    "max_leverage",
    "net_leverage",
    "period_close",
    "period_label",
    "period_open",
    "pnl",
    "portfolio_value",
    "positions",
    "returns",
    "short_exposure",
    "short_value",
    "shorts_count",
    "sorting",
    "starting_cash",
    "starting_exposure",
    "starting_value",
    "trading_days",
    "treasury_period_return",
]


def run_strategy(strategy_name):
    mod = None

    if strategy_name == "buy_and_hold":
        mod = BuyAndHold()

    register_calendar("YAHOO", get_calendar("NYSE"), force=True)

    return run_algorithm(
        initialize=getattr(mod, "initialize", None),
        handle_data=getattr(mod, "handle_data", None),
        before_trading_start=getattr(mod, "before_trading_start", None),
        analyze=getattr(mod, "analyze", None),
        bundle="quandl",
        environ=environ,
        **merge({"capital_base": 1e3}, mod._test_args())
    )

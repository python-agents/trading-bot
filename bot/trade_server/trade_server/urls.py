"""trade_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from server_service import views as s_views


urlpatterns = [
    path("admin/", admin.site.urls),
    path("stocks/", s_views.get_stocks, name="get_stocks"),
    path("get-state/<slug:stock>/<slug:user>", s_views.get_state, name="get_state"),
    path("bot/start", s_views.bot_start, name="bot_start"),
    path("bot/stop", s_views.bot_stop, name="bot_stop"),
    path("get-price/<slug:stock>", s_views.get_price, name="get_price"),
    path("update-wallet", s_views.update_wallet, name="update_wallet"),
    path("last30/<slug:stock>", s_views.get_last_month, name="get_last_month"),
]

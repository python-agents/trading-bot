import logging

from apscheduler.schedulers.background import BackgroundScheduler
from classes.live_trading.DataFetcher import DataFetcher


DOWNLOAD_MSG = "Downloading fresh data from last 30 days for %s"


def row_to_model(row):
    return {"date": str(row[0].date()), "value": row[1]}


def save_to_db(db, data, fields):
    if len(fields) == 1:
        db.update_stock_field(fields[0], "history", list(map(row_to_model, data.iteritems())))
    else:
        for column in fields:
            items = data[column].fillna(0).iteritems()
            db.update_stock_field(column, "history", list(map(row_to_model, items)))


def start(db):
    def runner():
        fields = db.get_stocks_short_names()

        logging.warning(DOWNLOAD_MSG % fields)
        if len(fields) > 0:
            data = DataFetcher.stocks_closing_price(fields)
            save_to_db(db, data, fields)

    runner()
    scheduler = BackgroundScheduler()
    scheduler.add_job(runner, "cron", day_of_week="mon-fri", hour=15, minute=31)
    scheduler.start()

import json
import threading

from classes.botdatabase import Database
from classes.live_trading.DataFetcher import DataFetcher
from classes.live_trading.TradeBot import TradeBot
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt


"""from django.shortcuts import render"""

bot_list = {}


def set_ddatabase(database):
    global botdb
    botdb = database


# Create your views here.
@csrf_exempt
def get_stocks(request):
    response = {"stocks": botdb.find_stock_info()}
    return JsonResponse(response, safe=False)


@csrf_exempt
def get_state(request, stock, user):
    response = botdb.find_state(stock, user)
    return JsonResponse(response, safe=False)


@csrf_exempt
def bot_start(request):
    if request.method == "POST":
        body_unicode = request.body.decode("utf-8")
        post_body = json.loads(body_unicode)
        if post_body["user"] in bot_list:
            return HttpResponseBadRequest("Bot already running")
        stock_list = [stock for stock in post_body["short_stock"]]
        bot_manager = TradeBot(
            stock_list, post_body["user"], post_body["short_stock"], botdb, post_body["strategy"]
        )
        thread = threading.Thread(target=bot_manager.run)
        thread.setDaemon(True)
        bot_list[post_body["user"]] = thread
        bot_list[thread] = bot_manager
        thread.start()
        botdb.update_bot_state(post_body["user"], bot_type=post_body["strategy"])
        return HttpResponse("Bot started", status=202)
    return HttpResponseBadRequest("Bad request, POST requred")


@csrf_exempt
def bot_stop(request):
    if request.method == "POST":
        body_unicode = request.body.decode("utf-8")
        post_body = json.loads(body_unicode)
        if post_body["user"] in bot_list:
            thread = bot_list[post_body["user"]]
            bot_manager = bot_list[thread]
            bot_manager.stop()
            thread.join()
            del bot_list[post_body["user"]]
            botdb.update_bot_state(post_body["user"])
            return HttpResponse("Bot stopped")
        else:
            return HttpResponseBadRequest("Working bot not found")
    return HttpResponseBadRequest("Bad request, POST requred")


@csrf_exempt
def get_price(request, stock):
    db = Database()
    price = DataFetcher.live_buying_price(stock)
    close = db.get_last_closing_price(stock)
    if price == 0:
        price = close
    change = (price / close) - 1.0
    return JsonResponse({"current_price": price, "change": change}, safe=False)


@csrf_exempt
def update_wallet(request):
    if request.method == "POST":
        post_body = json.loads(request.body)
        if post_body["user"] in bot_list:
            thread = bot_list[post_body["user"]]
            bot_manager = bot_list[thread]
            bot_manager.update_wallet(post_body["new_wallet"])
        else:
            old_wallet = botdb.get_wallet_value(post_body["user"])
            new_wallet = old_wallet + old_wallet
            botdb.update_wallet(post_body["user"], new_wallet)
    return JsonResponse({"wallet", "updated"})


@csrf_exempt
def get_last_month(request, stock):
    closeData = botdb.get_last_month_history(stock)
    closeData = list(filter(lambda stats: stats["value"], closeData))
    return JsonResponse(closeData, safe=False)

from classes.botdatabase import Database
from django.apps import AppConfig


class ServerServiceConfig(AppConfig):
    name = "server_service"

    def ready(self):
        from . import updater
        from . import views

        server_database = Database()
        views.set_ddatabase(database=server_database)
        updater.start(server_database)

# Trading Bot app

Trading Bot app is an platform for investing money in stocks. It uses trading bots to trade in market cap.

## Instalation 

### Prerequirements

- [Docker](https://docs.docker.com/get-docker/)

# Start with:
```terminal
docker-compose up --build
```

# Inspect in database 

With example of `.env` file run:
```bash
docker exec -it trades mongo -u "admin" -p "password" HOSTIP --authenticationDatabase "admin"
```

# Authors

* Kacper Wołowiec
* Barłomiej Ratajczak
* Karol Olszański
* Damian Kokot
